let ARTICLES = [],
    TEMPLATE = /*html*/ `<tr>${document.querySelector('[data-role="template"]').innerHTML}</tr>`,
    KEY = 'dacfc304a1814226b570068e0194b595'

$(document).ready(function() {

    setTimeout(function() {
        getNews()
    }, 2500)
})

function getNews() {
    var url = 'https://newsapi.org/v2/everything?' +
         'q=Apple&' +
         'apiKey=' + KEY;
     fetch(url, { mode: 'no-cors' }).then((resp) => {
         return resp.json()
     }).then((data) => {
         console.log(data);
     })

    /**
     * Les comptes gratuits n'ont pas acces a l'api
     * Du coup j'ai du improvisé
     */


    if (!_JSON.articles.length) {
        $('[data-role="empty-list"]').removeClass('d-none')
    }

    let i = 0
    _JSON.articles.forEach(article => {
        $('#list-articles').append(TEMPLATE
            .replace('{{id}}', ++i)
            .replace('{{title}}', article.title)
            .replace('{{language}}', article.source.name) // L'objet renvoyé ne donne aucune information concernant la langue
        )
    })
    $('[data-role="spinner"]').addClass('d-none')
}


const _JSON = {
    "status": "ok",
    "totalResults": 66349,
    "articles": [{
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Igor Bonifacic",
            "title": "Apple is reportedly working on a pay later feature for Apple Pay",
            "description": "If you’ve done any online shopping in the last little while, there’s a good chance you’ve run into services like Affirm and PayPal’s Pay in 4. They allow you to purchase something and pay for it later by splitting up the total cost of the item into several in…",
            "url": "https://www.engadget.com/apple-pay-later-report-205333095.html",
            "urlToImage": "https://s.yimg.com/os/creatr-images/2019-10/75fb6350-f5d1-11e9-8dbf-0806845f5eac",
            "publishedAt": "2021-07-13T20:53:33Z",
            "content": "If youve done any online shopping in the last little while, theres a good chance youve run into services like Affirm and PayPals Pay in 4. They allow you to purchase something and pay for it later by… [+1131 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Igor Bonifacic",
            "title": "Apple brings its free tutorials to YouTube",
            "description": "Since 2017, all of Apple's retail stores have offered Today At Apple sessions, free workshops where you can learn, among other things, how to get the most out of the company's devices and software. In 2020, necessitated by the pandemic, Apple started offering…",
            "url": "https://www.engadget.com/today-at-apple-youtube-130046204.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-07/359dc9e0-e497-11eb-bbef-2cb7fea1b072",
            "publishedAt": "2021-07-14T13:00:46Z",
            "content": "Since 2017, all of Apple's retail stores have offered Today At Apple sessions, free workshops where you can learn, among other things, how to get the most out of the company's devices and software. I… [+1226 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Kris Holt",
            "title": "HomePods get spatial audio and Apple Music lossless support in latest beta",
            "description": "It looks like HomePod\r\n and HomePod mini\r\n will soon have support for spatial audio and lossless audio via Apple Music. The HomePod 15 beta 5 includes options that enable lossless and Dolby Atmos\r\n playback.Not all beta users will see the toggles, according t…",
            "url": "https://www.engadget.com/homepod-mini-spatial-audio-apple-music-lossless-support-beta-210300048.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2020-11/74c373a1-244d-11eb-af8f-77d21fbd118b",
            "publishedAt": "2021-08-12T21:03:00Z",
            "content": "It looks like HomePod\r\n and HomePod mini\r\n will soon have support for spatial audio and lossless audio via Apple Music. The HomePod 15 beta 5 includes options that enable lossless and Dolby Atmos\r\n p… [+752 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Jon Fingas",
            "title": "Apple acknowledges 'confusion' over child safety updates",
            "description": "Apple is ready to acknowledge the controversy over its child safety updates, but it sees this as a matter of poor messaging — not bad policy. Senior software engineering VP Craig Federighi told the Wall Street Journal in an interview that introducing both the…",
            "url": "https://www.engadget.com/apple-on-confusion-over-child-safety-updates-csam-150121989.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-08/95838b10-fc3e-11eb-b7ff-a4f65f41eba6",
            "publishedAt": "2021-08-13T15:01:21Z",
            "content": "Apple is ready to acknowledge the controversy over its child safety updates, but it sees this as a matter of poor messaging not bad policy. Senior software engineering VP Craig Federighi told the Wal… [+1600 chars]"
        },
        {
            "source": {
                "id": "wired",
                "name": "Wired"
            },
            "author": "Gilad Edelman",
            "title": "The Privacy Battle That Apple Isn’t Fighting",
            "description": "California has begun enforcing a browser-level privacy setting, but you still can’t find that option in Safari or iOS.",
            "url": "https://www.wired.com/story/global-privacy-control-apple/",
            "urlToImage": "https://media.wired.com/photos/60f9f9ec9c7cd5dcccd9ee28/191:100/w_1280,c_limit/security-global-privacy.jpg",
            "publishedAt": "2021-07-30T12:00:00Z",
            "content": "For at least a decade, privacy advocates dreamed of a universal, legally enforceable Do not track setting. Now, at least in the most populous state in the US, that dream has become a reality. So why … [+3978 chars]"
        },
        {
            "source": {
                "id": "the-verge",
                "name": "The Verge"
            },
            "author": "Andrew Webster",
            "title": "Nobuo Uematsu’s beautiful Fantasian soundtrack is now on Apple Music",
            "description": "Longtime Final Fantasy composer Nobuo Uematsu has just released the soundtrack for the Apple Arcade RPG Fantasian, which is now available to stream on Apple Music.",
            "url": "https://www.theverge.com/2021/7/21/22586744/fantasian-soundtrack-apple-music-nobuo-uematsu",
            "urlToImage": "https://cdn.vox-cdn.com/thumbor/6nGGG5c6woCK2ZzfRWzMw2DXY0s=/0x75:3840x2085/fit-in/1200x630/cdn.vox-cdn.com/uploads/chorus_asset/file/22339470/1_4k.png",
            "publishedAt": "2021-07-21T13:29:51Z",
            "content": "One of the most exciting things about Fantasian — a Final Fantasy-style roleplaying game that debuted on Apple Arcade earlier this year — was how it reunited two industry legends. Hironobu Sakaguchi … [+1225 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Lifehacker.com"
            },
            "author": "Jake Peterson",
            "title": "How to Finally FaceTime Your Non-Apple Friends",
            "description": "If you’re an Apple devotee, there’s a good chance the company’s Messages and FaceTime services are two of the key features that keep you locked into the Cupertino ecosystem. But now, Apple has opened the floodgates just a crack, allowing us to invite anyone t…",
            "url": "https://lifehacker.com/how-to-finally-facetime-your-non-apple-friends-1847450728",
            "urlToImage": "https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/5f2d7125efb016fd60ef9f83d36f3c83.jpg",
            "publishedAt": "2021-08-10T14:00:00Z",
            "content": "If youre an Apple devotee, theres a good chance the companys Messages and FaceTime services are two of the key features that keep you locked into the Cupertino ecosystem. But now, Apple has opened th… [+2117 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Lifehacker.com"
            },
            "author": "Pranay Parab",
            "title": "How to Listen to Lossless Audio on Apple Music",
            "description": "Apple Music allows you to listen to some songs and albums in lossless audio quality, which—as the name suggests—are extremely high quality audio files. And because you don’t have to pay extra for the feature (it’s included with the $9.99 per month plan), if y…",
            "url": "https://lifehacker.com/how-to-listen-to-lossless-audio-on-apple-music-1847349371",
            "urlToImage": "https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/da755de0cef0e312848311445f4e6513.jpg",
            "publishedAt": "2021-07-23T17:30:00Z",
            "content": "Apple Music allows you to listen to some songs and albums in lossless audio quality, whichas the name suggestsare extremely high quality audio files. And because you dont have to pay extra for the fe… [+3497 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Jon Fingas",
            "title": "Apple pulls anti-vax social app over misinformation",
            "description": "Mobile app shops are cracking down on one of the higher-profile communities spreading anti-vax misnformation. Bloombergreports that Apple has removed Unjected, a hybrid social and dating app for the unvaccinated, for \"inappropriately\" referencing the COVID-19…",
            "url": "https://www.engadget.com/apple-pulls-unjected-anti-vax-app-172231369.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-05/2e05ce90-be6c-11eb-8fb5-642cfc0fd443",
            "publishedAt": "2021-07-31T17:22:31Z",
            "content": "Mobile app shops are cracking down on one of the higher-profile communities spreading anti-vax misnformation. Bloombergreports that Apple has removed Unjected, a hybrid social and dating app for the … [+1370 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Jon Fingas",
            "title": "PlayStation 5 owners now get six free months of Apple TV+",
            "description": "Apple is clearly determined to pump up TV+ viewership now that more Ted Lasso is on the way, including on other platforms. PlayStation 5 owners now get six months of free Apple TV+ service, whether they're newcomers or existing subscribers (except for Apple O…",
            "url": "https://www.engadget.com/playstation-5-apple-tv-plus-six-months-free-trial-152055045.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-07/dc2025b0-eafa-11eb-93ec-2f87c2c25315",
            "publishedAt": "2021-07-22T15:20:55Z",
            "content": "Apple is clearly determined to pump up TV+ viewership now that more Ted Lasso is on the way, including on other platforms. PlayStation 5 owners now get six months of free Apple TV+ service, whether t… [+963 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Kris Holt",
            "title": "‘Castlevania: Grimoire of Souls’ will soon be revived on Apple Arcade",
            "description": "A new(ish) Castlevania\r\n game is on the way to Apple Arcade. Castlevania: Grimoire of Souls is coming soon to iOS, tvOS and Mac via the subscription service. Konami announced Castlevania: Grimoire of Souls\r\n in 2018. It soft launched the game on iOS in Canada…",
            "url": "https://www.engadget.com/castlevania-grimoire-of-souls-apple-arcade-174946430.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-08/c7b68330-fc5c-11eb-babf-f342811e876e",
            "publishedAt": "2021-08-13T17:49:46Z",
            "content": "A new(ish) Castlevania\r\n game is on the way to Apple Arcade. Castlevania: Grimoire of Souls is coming soon to iOS, tvOS and Mac via the subscription service. Konami announced Castlevania: Grimoire of… [+976 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Jon Fingas",
            "title": "Apple Watch Series 6 Product Red drops to $265 at Amazon",
            "description": "Now might be a good time to buy the Apple Watch Series 6 — at least, if you're fond of red. Amazon is selling the 40mm Product Red edition of the Apple smartwatch for just $265 at checkout, well below the official $399 price. That's lower than the price we sa…",
            "url": "https://www.engadget.com/apple-watch-series-6-product-red-amazon-sale-151433869.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-07/c55fd260-ec8c-11eb-b7af-fcf1d94f9d2c",
            "publishedAt": "2021-07-24T15:14:33Z",
            "content": "Now might be a good time to buy the Apple Watch Series 6 at least, if you're fond of red. Amazon is selling the 40mm Product Red edition of the Apple smartwatch for just $265 at checkout, well below … [+1239 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Andrew Tarantola",
            "title": "Apple clarifies its sex abuse scans would look for 'images flagged in multiple countries'",
            "description": "Apple, which basked in ubiquitous praise after refusing to cooperate with federal authorities following the 2016 mass shooting in San Bernardino, California, is now aggressively walking back its plans to unilaterally scan customers' phones for child porn on b…",
            "url": "https://www.engadget.com/apple-clarifies-its-sex-abuse-scans-would-look-for-images-flagged-in-multiple-countries-232229749.html",
            "urlToImage": "https://s.yimg.com/os/creatr-images/2020-04/d1392330-80da-11ea-9eeb-613195e7f73b",
            "publishedAt": "2021-08-13T23:22:29Z",
            "content": "Apple, which basked in ubiquitous praise after refusing to cooperate with federal authorities following the 2016 mass shooting in San Bernardino, California, is now aggressively walking back its plan… [+1942 chars]"
        },
        {
            "source": {
                "id": "techcrunch",
                "name": "TechCrunch"
            },
            "author": "Zack Whittaker",
            "title": "Apple says it will begin scanning iCloud Photos for child abuse images",
            "description": "Later this year, Apple will roll out a technology that will allow the company to detect and report known child sexual abuse material to law enforcement in a way it says will preserve user privacy. Apple told TechCrunch that the detection of child sexual abuse…",
            "url": "http://techcrunch.com/2021/08/05/apple-icloud-photos-scanning/",
            "urlToImage": "https://techcrunch.com/wp-content/uploads/2021/08/icloud-pattern-with-key.jpg?w=615",
            "publishedAt": "2021-08-05T19:00:06Z",
            "content": "Later this year, Apple will roll out a technology that will allow the company to detect and report known child sexual abuse material to law enforcement in a way it says will preserve user privacy.\r\nA… [+5495 chars]"
        },
        {
            "source": {
                "id": null,
                "name": "Lifehacker.com"
            },
            "author": "Jake Peterson",
            "title": "How to Finally 'Instant Transfer' Your Apple Cash to a Visa and MasterCard",
            "description": "“It’s my money, and I need it now!” You said it, JG Wentworth. The cash back you rack up when making purchases on your Apple Card is yours to do with as you like—but if you want to send it to your bank account, it’ll take one to three business days to get the…",
            "url": "https://lifehacker.com/how-to-finally-instant-transfer-your-apple-cash-to-a-vi-1847436460",
            "urlToImage": "https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/9ac091d8aa83f0aa1d4a936080ccba64.jpg",
            "publishedAt": "2021-08-06T17:00:00Z",
            "content": "Its my money, and I need it now! You said it, JG Wentworth. The cash back you rack up when making purchases on your Apple Card is yours to do with as you likebut if you want to send it to your bank a… [+2625 chars]"
        },
        {
            "source": {
                "id": "techcrunch",
                "name": "TechCrunch"
            },
            "author": "Amanda Silberling",
            "title": "Apple News partners with NBCUniversal to share exclusive Olympics content",
            "description": "It’s hard to keep up with the Olympics any year, let alone when they’re taking place in a time zone that’s 13 hours ahead of you, if you’re on the East Coast. Yesterday, Apple News announced a collaboration with NBCUniversal (the U.S. broadcast rights holder …",
            "url": "http://techcrunch.com/2021/07/27/apple-news-partners-with-nbcuniversal-to-share-exclusive-olympics-content/",
            "urlToImage": "https://techcrunch.com/wp-content/uploads/2020/02/apple-news-ios-icon.jpg?w=711",
            "publishedAt": "2021-07-27T15:28:05Z",
            "content": "It’s hard to keep up with the Olympics any year, let alone when they’re taking place in a time zone that’s 13 hours ahead of you (if you’re on the U.S. East Coast). Apple News on Monday announced a c… [+1805 chars]"
        },
        {
            "source": {
                "id": "techcrunch",
                "name": "TechCrunch"
            },
            "author": "Darrell Etherington",
            "title": "Apple drops its lawsuit against maker of iPhone emulation software",
            "description": "Apple has settled its 2019 lawsuit with Corellium, a company that builds virtual iOS devices used by security researchers to find bugs in iPhones and other iOS devices.",
            "url": "http://techcrunch.com/2021/08/11/apple-drops-its-lawsuit-against-maker-of-iphone-emulation-software/",
            "urlToImage": "https://techcrunch.com/wp-content/uploads/2021/08/0639e4eb324544a0a52ad480bf11e3da1.jpg?w=600",
            "publishedAt": "2021-08-11T13:53:49Z",
            "content": "More posts by this contributor\r\nApple has settled its 2019 lawsuit with Corellium, a company that builds virtual iOS devices used by security researchers to find bugs in iPhones and other iOS devices… [+1406 chars]"
        },
        {
            "source": {
                "id": "the-verge",
                "name": "The Verge"
            },
            "author": "Jay Peters",
            "title": "Apple just launched an official $99 MagSafe battery pack for the iPhone 12 lineup",
            "description": "Apple has launched the MagSafe Battery Pack, a magnetic battery pack that you can stick onto the back of your iPhone 12. It costs $99.",
            "url": "https://www.theverge.com/2021/7/13/22575678/apple-magsafe-battery-pack-iphone-12-pro-mini-max",
            "urlToImage": "https://cdn.vox-cdn.com/thumbor/Dm5sZM1bPav_7cL6SWxPu5iwAM8=/0x177:2052x1251/fit-in/1200x630/cdn.vox-cdn.com/uploads/chorus_asset/file/22715100/Screen_Shot_2021_07_13_at_9.56.00_AM.jpg",
            "publishedAt": "2021-07-13T17:07:02Z",
            "content": "Available now from Apple\r\nIf you buy something from a Verge link, Vox Media may earn a commission. See our ethics statement.\r\nApples new MagSafe Battery Pack.\r\nImage: Apple\r\nApple has launched the Ma… [+888 chars]"
        },
        {
            "source": {
                "id": "the-verge",
                "name": "The Verge"
            },
            "author": "Chaim Gartenberg",
            "title": "Matter’s interoperable smart home standard has been delayed to 2022",
            "description": "Matter is a smart home standard that aims to unite Apple, Google, and Amazon’s different standards for simpler setup and configuration, but it’s now delayed to 2022.",
            "url": "https://www.theverge.com/2021/8/13/22623275/matter-interoperable-smart-home-standard-delay-2022-project-chip-csa",
            "urlToImage": "https://cdn.vox-cdn.com/thumbor/jsOel-l1vlo7is2YM5NHLNPNfOw=/26x0:2104x1088/fit-in/1200x630/cdn.vox-cdn.com/uploads/chorus_asset/file/22506858/matter.png",
            "publishedAt": "2021-08-13T16:29:38Z",
            "content": "Another delay for the Amazon, Google, and Apple-bridging standard\r\nMatter the upcoming smart home standard that looks to help unify Google, Apple, and Amazons separate smart home systems into a singl… [+1961 chars]"
        },
        {
            "source": {
                "id": "engadget",
                "name": "Engadget"
            },
            "author": "Mariella Moon",
            "title": "Apple Music's lossless and spatial audio streaming arrive on Android devices",
            "description": "Apple has recently updated its Music app for Android, but it left out a couple of new features you may have been waiting for: support for lossless streaming and spatial audio. Engadget has confirmed that the tech giant has started rolling out the new high-qua…",
            "url": "https://www.engadget.com/apple-music-lossless-spatial-audio-streaming-android-034510410.html",
            "urlToImage": "https://s.yimg.com/os/creatr-uploaded-images/2021-07/634f0ee0-eb62-11eb-ae57-34551c2d1e28",
            "publishedAt": "2021-07-23T03:45:10Z",
            "content": "Apple has recently updated its Music app for Android, but it left out a couple of new features you may have been waiting for: support for lossless streaming and spatial audio. Engadget has confirmed … [+1190 chars]"
        }
    ]
}