let ARTICLES = [],
    TEMPLATE = /*html*/ `<tr>${document.querySelector('[data-role="template"]').innerHTML}</tr>`

let nb = 0

/**
 * Ajout d'un nouvel article
 */
function addNewArticle() {
    const modal = $('#modal-article'),
        form = modal.find('form')

    modal.find('.modal-title').text('Ajouter un article')
    form.find('.hidden').val('add')

    modal.modal('show')
    form.submit(function(e) {
        e.preventDefault();
        if (nb) {
            return
        }
        if (form.find('.hidden').val() !== 'add') {
            return
        }
        const data = getParsedBody($(this))

        if (!hasError(data)) {
            modal.modal('hide')
            const idArticle = ARTICLES.length + 1
            $('#list-articles').append(TEMPLATE
                .replace(/{{id}}/g, idArticle)
                .replace('{{categorie}}', data.categorie_article)
                .replace('{{nom}}', data.nom_article)
                .replace('{{prix}}', data.prix_article))

            ARTICLES.push(Object.assign({}, data, { idArticle }))

            toggleEmptyMessage()
            nb++
        }
    })
    modal.on('hidden.bs.modal', function() {
        nb = 0
        form.get(0).reset()
        form.find('.invalid-feedback').addClass('text-hide')
    })
}


function editArticle(id) {
    const row = $(`td[data-elt-id="${id}"]`).parent(),
        modal = $('#modal-article'),
        form = modal.find('form')

    modal.find('.modal-title').text("Modifier l'article")
    form.find('.hidden').val('edit')
    form.find('select[name="categorie_article"]').val(row.find('td:eq(0)').text())
    form.find('input[name="nom_article"]').val(row.find('td:eq(1)').text())
    form.find('input[name="prix_article"]').val(row.find('td:eq(2)').text())


    modal.modal('show')
    form.submit(function(e) {
        e.preventDefault();
        if (nb) {
            return
        }
        if (form.find('.hidden').val() !== 'edit') {
            return
        }
        const data = getParsedBody($(this))

        if (!hasError(data)) {
            row.find('td:eq(0)').text(data.categorie_article)
            row.find('td:eq(1)').text(data.nom_article)
            row.find('td:eq(2)').text(data.prix_article)
            nb++
            modal.modal('hide')
        }
    })
    modal.on('hidden.bs.modal', function() {
        nb = 0
        form.get(0).reset()
        form.find('.invalid-feedback').addClass('text-hide')
    })
}

function deleteArticle(id) {
    const modal = $('#modal-delete-article')

    modal.find('span[data-role="nom_article"]').text(
        ARTICLES.filter(elt => elt.idArticle == id)[0].nom_article
    )

    modal.modal('show').on('shown.bs.modal', function() {
        modal.find('button[type="submit"]').click(function() {
            $(`td[data-elt-id="${id}"]`).parent().remove()
            ARTICLES = ARTICLES.filter(elt => elt.idArticle != id)

            modal.modal('hide')
            toggleEmptyMessage()
        })
    })
}

/**
 * 
 * @param {jQuery Element} form 
 * @return {Object}
 */
function getParsedBody(form) {
    const raw = form.serializeArray()
    let data = {}

    for (let i = 0; i < raw.length; i++) {
        const element = raw[i];
        data = Object.assign({}, data, {
            [element.name]: element.value
        })
    }

    return data
}

/**
 * Verifie les champs vide
 * 
 * @param {object} data 
 * @return {Boolean}
 */
function hasError(data) {
    let error = false
    for (let key in data) {
        if (data[key] == '') {
            error = true
            $('#' + key).parent().find('span').removeClass('text-hide')
        } else {
            $('#' + key).parent().find('span').addClass('text-hide')
        }
    }
    return error
}

function toggleEmptyMessage() {
    if (ARTICLES.length) {
        $('[data-role="empty-list"]').addClass('d-none')
    } else {
        $('[data-role="empty-list"]').removeClass('d-none')
    }
}